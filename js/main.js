
(function () {

  // Определение вызывающего элемента
  var getTriggerElement = function (el) {
    // Получаем атрибут `data-collapse`
    var isCollapse = el.getAttribute('data-collapse');
    // Если атрибут существует, то
    if (isCollapse !== null) {
      // Возвращаем элемент на котором осуществлен клик
      return el;
    } else {
      // Иначе пытаемся найти атрибут у его родителя
      var isParentCollapse = el.parentNode.getAttribute('data-collapse');
      // Возвращаем родительский элемент или undefined
      return (isParentCollapse !== null) ? el.parentNode : undefined;
    }
  };

  // Обработчик клика
  var collapseClickHandler = function (event) {
    // Определение вызывающего элемента
    var triggerEl = getTriggerElement(event.target);
    // Если у элемента и его родителя нет атрибута
    if (triggerEl === undefined) {
      // Отменяем действие
      return false;
    } else {
      event.preventDefault();
    }

    // Получаем целевой элемент
    var targetEl = document.querySelector(triggerEl.getAttribute('data-target'));
    // Если целевой элемент существует
    if (targetEl) {
      // Манипулируем классами
      triggerEl.classList.toggle('-active');
      targetEl.classList.toggle('-on');
    }
  };



  // Делегируем событие
  document.addEventListener('click', collapseClickHandler, false);

  //


})(document, window);


(function () {
  var menuItems = document.getElementsByClassName("navbar-component__link");
var url = document.location.href;
for (var i = 0; i < menuItems.length;i++) {
  if (menuItems[i].href == url) {
    menuItems[i].classList.add("navbar-component__link--active")
  }
}
})(document, window);


$('.home-slider').slick({
  appendDots: $(".home-slider__dots"),
  vertical:false,
  responsive: [
    {
      breakpoint: 544,
      settings: {
        vertical : true
      }
    }]
});
$('.testimonals-slider').slick({
  appendDots: $(".testimonals-slider__dots")
});
$('.portfolio-slider').slick({
  appendDots: $(".portfolio-slider__dots"),
  responsive: [
    {
      breakpoint: 992,
      settings: {
        rows: 2,
        slidesPerRow: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        rows: 2,
        slidesPerRow: 2,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 554,
      settings: {
        rows: 2,
        slidesToShow: 1,
        slidesPerRow: 1,
        infinite: true,
        dots: true
      }
    }
  ]
});



// $(document).ready(function () {
//   $('.home-slider').slick({
//     appendDots: $(".home-slider__dots")
//   });
//   $('.testimonals-slider').slick({
//     appendDots: $(".testimonals-slider__dots")
//   });
//   $('.portfolio-slider').slick({
//     appendDots: $(".portfolio-slider__dots")
//   });
// });


var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var posthtml = require('gulp-posthtml');
var include = require('posthtml-include');
var autoprefixer = require('autoprefixer');
var server = require('browser-sync').create();
var minify = require('gulp-csso');
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin');
var run = require('run-sequence');
var del = require('del');
var uglifyjs = require('gulp-uglify');
var pump = require('pump');
var concat = require('gulp-concat');
var spritesmith = require('gulp.spritesmith');


// tasks

gulp.task('html', function() {
  return gulp.src('*.html')
    .pipe(posthtml([
      include()
    ]))
    .pipe(gulp.dest('build'))
    .pipe(server.stream());
})

gulp.task('style', function() {
  gulp.src('sass/style.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(postcss([
      autoprefixer()
    ]))
    .pipe(gulp.dest('build/css'))
    .pipe(minify())
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('build/css'))
    .pipe(server.stream())

});


gulp.task('serve', function(){
  server.init({
    server : 'build/',
    notify : false,
    open : true,
    cors : true,
    ui : false
  });

  gulp.watch('sass/**/*.scss', ['style']);
  gulp.watch('*.html', ['html'])
});

gulp.task('uglify', function (cb) {
  pump([
        gulp.src([
          'node_modules/jquery/dist/jquery.min.js',
          'node_modules/slick-carousel/slick/slick.min.js',
          'js/main.js',
        ])
        .pipe(concat('main.min.js'))
        .pipe(uglifyjs()),
        gulp.dest('build/js')
    ],
    cb
  );
});

gulp.task('copy', function() {
  return gulp.src([
    'fonts/**/*.{woff,woff2}',
    'img/**/*.{jpg,webp,png}',
    'js/**'
  ], {
    base: '.'
  })
  .pipe(gulp.dest('build'));
});

gulp.task('sprite', function () {
  var spriteData = gulp.src('sprite/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.scss',
    cssTemplate: './sass/scss.template.handlebars',
    imgPath: "../img/sprite.png"
  }));

  spriteData.img.pipe(gulp.dest('./build/img')); // путь, куда сохраняем картинку
  spriteData.css.pipe(gulp.dest('./sass/')); // путь, куда сохраняем стили
});

gulp.task('clean', function() {
  return del('build');
});

gulp.task('build', function(done) {
  run('clean', 'copy', 'sprite', 'style', 'uglify', 'html', done);
})
